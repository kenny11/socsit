<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests;
use App\Post;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('comment.privacy', ['only' => [
            'show',
            'edit',
        ]]);
        $this->middleware('comment.author', ['only' => [
            'update',
            'edit',
            'destroy',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $comments = Comment::where('user_id','=',Auth::id())->get();
        return $this->responseSuccessWithData('comments', $comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('api.comment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreCommentRequest $request)
    {

    	$input = $request->all();
    	$post = Post::findOrFail($input['post_id']);
        $comment = new Comment();
        $comment->text = $input['text'];
        $comment->post_id = $input['post_id'];
		$comment->user_id = Auth::id();
		$comment->save();
		return $this->responseSuccessWithData('comment', $comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    	$comment = Comment::findOrFail($id);
    	return $this->responseSuccessWithData('comment', $comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$comment = Comment::findOrFail($id);
		return view('api.comment.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(StoreCommentRequest $request, $id)
    {
    	$input = $request->input();
        $comment = Comment::findOrFail($id);
		//Zjisti jestli comment upravuje jeho vlastnik
		if(Auth::id()!=$comment->user_id)
			return $this->responseJSON('error', 'message', 'unauthorized', 403);
		$comment->text = $input['text'];
		$comment->save();
		return $this->responseSuccessWithData('comment', $comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
		//Zjisti jestli comment upravuje jeho vlastnik
		if(Auth::id()!=$comment->user_id)
			return $this->responseUnauthorized();
		$comment->delete();
		return $this->responseSuccess('successfully deleted');
    }
}

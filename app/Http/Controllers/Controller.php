<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
	
	/**
     * Display response.
     *
     * @param  string  $status
     * @param  string  $dataName
     * @param  string  $data
     * @param  int  $statusCode
     * @return Response
     */
    private function responseJSON($status, $dataName, $data, $statusCode)
    {
    	return response()->json(['status' => $status, $dataName => $data], $statusCode);
    }
    /**
     * Display error response.
     * 
     * @param  string  $message
     * @param  int  $statusCode
     * @return Response
     */
    protected function responseError($message, $statusCode)
    {
        return $this->responseJSON('error', 'message', $message, $statusCode);
    }
    /**
     * Display unauthorized error response.
     *
     * @return Response
     */
    protected function responseUnauthorized()
    {
        return $this->responseJSON('error', 'message', 'unauthorized', 403);
    }
    /**
     * Display successfull response with message.
     *
     * @param  string  $message
     * @return Response
     */
    protected function responseSuccess($message)
    {
        return $this->responseJSON('success', 'message', $message, 200);
    }
    /**
     * Display successfull response with data.
     *
     * @param  string  $dataName
     * @param  string  $data
     * @return Response
     */
    protected function responseSuccessWithData($dataName, $data)
    {
        return $this->responseJSON('success', $dataName, $data, 200);
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFriendRequest;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;

class FriendController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $user = Auth::user();
    	$friends = $user->getAllFriendships();
        return $this->responseSuccessWithData('friends', $friends);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreFriendRequest $request)
    {
        $recipient = $request->input()['recipient'];
        $friend = User::findOrFail($recipient);
        $user = Auth::user();
        if($user->isFriendWith($friend))
            return $this->responseError('already friends', 400);
        $friendship = $user->befriend($friend);
		return $this->responseSuccessWithData('friend', $friendship);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $friend = User::findOrFail($id);
        try 
        {
            $user->unfriend($id);
            return $this->responseSuccess('successfully removed friend');
        } 
        catch (\Exception $e) 
        {
            return $this->responseError('can\'n remove friend', 500);
        }
    }

    /**
     * Accept friend request
     *
     * @param  int  $id
     * @return Response
     */
    public function acceptFriend($id)
    {
        $user = Auth::user();
        $friend = User::findOrFail($id);
        try 
        {
            if($user->acceptFriendRequest($friend) == 0)
                abort(400); 
            return $this->responseSuccess('successfully accepted friend request');
        } 
        catch (\Exception $e) 
        {
            return $this->responseError('can\'t accept friend request', 500);
        }
    }

    /**
     * Deny friend request
     *
     * @param  int  $id
     * @return Response
     */
    public function denyFriend($id)
    {
        $user = Auth::user();
        $friend = User::findOrFail($id);
        try 
        {
            if($user->denyFriendRequest($friend) == 0)
                abort(400);
            return $this->responseSuccess('successfully denied friend request');
        } 
        catch (\Exception $e) 
        {
            return $this->responseError('can\'n deny friend request', 500);
        }
    }

    /**
     * Deny friend request
     *
     * @param  int  $id
     * @return Response
     */
    public function blockFriend($id)
    {
        $user = Auth::user();
        $friend = User::findOrFail($id);
        try 
        {
            if($user->blockFriend($friend) == 0)
                abort(400);
            return $this->responseSuccess('successfully blocked friend');
        } 
        catch (\Exception $e) 
        {
            return $this->responseError('can\'n block friend', 500);
        }
    }

    /**
     * Get list of pending requests
     *
     * @param  int  $id
     * @return Response
     */
    public function listOfPending(Request $request, $direction = null)
    {
        $user = Auth::user();
        try 
        {
            if(isset($direction))
            {
                if($direction == 'received')
                    $requests = $user->getFriendRequests();
                else if($direction == 'sent')
                {
                    $all = $user->getPendingFriendships();
                    $received = $user->getFriendRequests();
                    $requests = $all->diff($received);
                }
                else
                    return $this->responseError('bad option, valid options are sent|received', 400);
            }
            else
            {
                $requests = $user->getPendingFriendships();
            }
            return $this->responseSuccessWithData('requests', $requests);
        } 
        catch (\Exception $e) 
        {
            return $this->responseError('can\'n get list of pending requests', 500);
        }
    }  

    /**
     * Get list of accepted friendships
     *
     * @param  int  $id
     * @return Response
     */
    public function listOfAccepted()
    {
        $user = Auth::user();
        try 
        {
            $friends = $user->getAcceptedFriendships();
            return $this->responseSuccessWithData('friends', $friends);
        } 
        catch (\Exception $e) 
        {
            return $this->responseError('can\'n get list of accepted friendships', 500);
        }
    } 
}

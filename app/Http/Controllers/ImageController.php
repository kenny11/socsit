<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Image;
use App\Http\Requests;
use App\Http\Requests\StoreImageRequest;
use App\Http\Requests\UpdateImageRequest;
use InterventionImage;

class ImageController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('image.privacy', ['only' => [
            'show',
            'edit',
        ]]);
        $this->middleware('image.author', ['only' => [
            'update',
            'edit',
            'destroy',
        ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $images = Image::where('user_id','=',Auth::id())->get();
        return $this->responseSuccessWithData('images', $images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('api.image.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreImageRequest $request)
    {
    	$input = $request->all();
    	if(!$request->file('image')->isValid())
    		return $this->responseError('upload error', 400);
    	$name = $this->availableName().'.'.$request->file('image')->extension();
    	try 
        {
    		$request->file('image')->move(config('image.path'), $name);
            if($input['type'] == 'profile')
            {
                $img = InterventionImage::make(config('image.path').'/'.$name);
                $size = $img->height() > $img->width() ? $img->width() : $img->height();
                $img->crop($size, $size)->save();
            }
    	} 
        catch (\Exception $e) 
        {
    		return $this->responseError('server error', 500);
    	}
        $image = new Image();
        $image->name = $name;
        $image->type = $input['type'];
        $image->privacy = $input['privacy'];
		$image->user_id = Auth::id();
		$image->save();
		return $this->responseSuccessWithData('image', $image);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    	$image = Image::findOrFail($id);
    	return $this->responseSuccessWithData('image', $image);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$image = Image::findOrFail($id);
		return view('api.image.edit', compact('image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
    	$input = $request->input();
        $image = Image::findOrFail($id);
        $image->type = $input['type'];
        $image->privacy = $input['privacy'];
		$image->save();
		return $this->responseSuccessWithData('image', $image);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $image = Image::findOrFail($id);
		$image->delete();
		return $this->responseSuccess('successfully deleted');
    }

    public function getImage($name)
    {
    	
    }

    private function availableName()
    {
    	$name = str_random(config('image.nameLength'));
    	$image = Image::where('name', $name);
    	while ($image->count()>0) 
    	{
    		$name = str_random(config('image.nameLength'));
    		$image = Image::where('name', $name);
    	}
    	return $name;
    }
}

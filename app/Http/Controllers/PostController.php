<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests;
use App\Post;
use App\Image;
use Auth;

class PostController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('post.privacy', ['only' => [
            'show',
            'edit',
        ]]);
        $this->middleware('post.author', ['only' => [
            'update',
            'edit',
            'destroy',
        ]]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::where('user_id','=',Auth::id())->get();
        return $this->responseSuccessWithData('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('api.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StorePostRequest $request)
    {
    	$input = $request->all();
        $post = new Post();
        $post->text = $input['text'];
        $post->type = $input['type'];
        if($post->type!='text')
			$post->data = $input['data'];
        $post->privacy = $input['privacy'];
		$post->user_id = Auth::id();
		$post->save();
		return $this->responseSuccessWithData('post', $post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    	$post = Post::findOrFail($id);
    	return $this->responseSuccessWithData('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$post = Post::findOrFail($id);
		return view('api.post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(StorePostRequest $request, $id)
    {
    	$input = $request->input();
        $post = Post::findOrFail($id);
		$post->text = $input['text'];
		$post->type = $input['type'];
		if($post->type!='text')
			$post->data = $input['data'];
        $post->privacy = $input['privacy'];
		$post->save();
		return $this->responseSuccessWithData('post', $post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        if($post->type == 'image')
        {
            $image = Image::find(json_decode($post->data)->image);
            $image->delete();
        }
		$post->delete();
		return $this->responseSuccess('successfully deleted');
    }
}

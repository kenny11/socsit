<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use App\Http\Requests\StoreThreadRequest;
use App\Http\Requests\UpdateThreadRequest;
use App\Http\Requests;

class ThreadController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        $threads = Thread::forUser(Auth::id())->latest('updated_at')->get();
        return $this->responseSuccessWithData('threads', $threads);
    }
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (\Exception $e) {
            return $this->responseError('thread not found', 404);
        }
        if($thread->hasParticipant(Auth::id()))
        {
        	$user = Auth::id();
        	$thread->markAsRead($user);
        	return $this->responseSuccessWithData('thread', $thread);
        }
        return $this->responseUnauthorized();
    }

    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        //$users = User::where('id', '!=', Auth::id())->get();  //All users
        $users = Auth::user()->getFriends(); //Only friends
        return view('api.thread.create', compact('users'));
    }
    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(StoreThreadRequest $request)
    {
        $input = $request->all();
        $thread = Thread::create(
            [
                'subject' => $this->getValidSubject($input),
            ]
        );
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );
        // Recipients
        if ($request->has('recipients')) {
            $thread->addParticipants($input['recipients']);
        }
        return $this->responseSuccessWithData('thread', $thread);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
		$thread = Thread::findOrFail($id);
		$users = Auth::user()->getFriends();
		$users = $users->diff($thread->participants);
		return view('api.thread.edit', compact('thread', 'users'));
    }

    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update(UpdateThreadRequest $request, $id)
    {
    	$input = $request->all();
        try {
            $thread = Thread::findOrFail($id);
        } catch (\Exception $e) {
            return $this->responseError('thread not found', 404);
        }
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => $input['message'],
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (isset($input['recipients'])) {
            $thread->addParticipants($input['recipients']);
        }
        return $this->responseSuccess('successfully sent message');
    }


    public function getThreadMessages($id, $start = 0, $count = 20)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (\Exception $e) {
            return $this->responseError('thread not found', 404);
        }
        if($thread->hasParticipant(Auth::id()))
        {
            $user = Auth::id();
            $thread->markAsRead($user);
            $messages = $thread->splice($start, $count);
            return $this->responseSuccessWithData('messages', $messages);
        }
        return $this->responseUnauthorized();
    }

    /**
     * Validate subject and return it
     *
     * @param $id
     * @return mixed
     */
    private function getValidSubject($input)
    {
    	if(isset($input['subject']) && $input['subject'] != '')
    		return $input['subject'];
        if($input['recipients'] != '')
        {
            $names = [];
            foreach ($input['recipients'] as $recipient) 
            {
                $name = User::find($recipient)->name;
                array_push($names, $name);
                
            }
            array_push($names, Auth::user()->name);
            $newSubject = implode(', ', $names);
            return $newSubject;
        }
        else
        	return '';
    }
}

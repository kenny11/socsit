<?php

namespace App\Http\Middleware;

use Closure;
use App\Comment;
use Auth;

class CommentPrivacy
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = $request->getPathInfo();
        $parts = explode('/', $path);
        foreach ($parts as $part) {
            if(ctype_digit($part))
                $id = $part;
        }
        $comment = Comment::findOrFail($id);
        if($comment->post->canSee())
            return $next($request);
        else
            return response()->json(['status' => 'error', 'message' => 'unauthorized'], 403);
    }
}

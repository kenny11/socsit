<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreImageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'     => 'image|dimensions:min_width=200,min_height=200',
            'type'      => 'required|in:post,profile',
            'privacy'   => 'required|privacyJSON',
        ];
    }
}

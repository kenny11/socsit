<?php

namespace App\Http\Requests;
use App\User;
use App\Http\Requests\Request;

class StoreThreadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'max:255',
            'message' => 'required|max:255',
            'recipients' => 'required'
        ];
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('main.welcome');
});
Route::auth();
Route::resource('post', 'PostController');
Route::resource('comment', 'CommentController');
Route::resource('image', 'ImageController');
Route::resource('thread', 'ThreadController');
Route::get('/thread/{id}/messages/{start?}/{count?}', 'ThreadController@getThreadMessages');
Route::resource('friend', 'FriendController', ['only' => ['index', 'store','destroy']]);
Route::group(['prefix' => 'friend'], function(){
	Route::get('/{id}/accept', 'FriendController@acceptFriend');
	Route::get('/{id}/deny', 'FriendController@denyFriend');
	Route::get('/pending/{direction?}', 'FriendController@listOfPending');
	Route::get('/accepted', 'FriendController@listOfAccepted');
});




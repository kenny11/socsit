<?php 
use Illuminate\Support\Facades\Request;

class Utils
{
	public static function dateToText($date) {
		$date = new DateTime($date);
		$now = new DateTime();
		$interval = $date->diff($now);
		if($interval->y > 0)
			return Lang::choice('site/time.year', $interval->y,['year' => $interval->y]);
		elseif($interval->m > 0)
			return Lang::choice('site/time.month', $interval->m,['month' => $interval->m]);
		elseif($interval->d > 7 )
			return Lang::choice('site/time.week', $interval->d % 7,['week' => $interval->d % 7]);
		elseif($interval->d > 0)
			return Lang::choice('site/time.day', $interval->d,['day' => $interval->d]);
		elseif($interval->h > 0)
			return Lang::choice('site/time.hour', $interval->h, ['hour' => $interval->h]);
		elseif($interval->i > 0)
			return Lang::choice('site/time.minute', $interval->i, ['minute' => $interval->i]);
		else return Lang::choice('site/time.seconds', $interval->s, ['second' => $interval->s]);
	}
	public static function pathToImage($name)
	{
		return URL::to(config('image.path').'/'.$name);
	}
	/*
	public static function setDisabled($name)
	{
	    return (Auth::check() && Auth::user()->can(str_replace('/', '.', $name)))? "" : "disabled";
	}

	public static function setActive($name)
	{
	    return (URL::getRequest()->is($name))? "active" : "";
	}*/
}

<?php

namespace App;

use App\Traits\Privacy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
	use SoftDeletes, Privacy;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['name', 'user_id', 'type', 'privacy'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

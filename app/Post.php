<?php

namespace App;

use App\Traits\Privacy;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Post extends Model
{
    use SoftDeletes, Privacy;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['text', 'type', 'data', 'user_id', 'privacy'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comments()
    {
		return $this->hasMany('App\Comment');
    }

    public function isAuthor($id = 0)
    {
        $id = $id != 0 ? $id : Auth::id();
        return $this->user_id == $id;
    }


    public function age()
    {
        $date = new DateTime($this->created_at);
        $now = new DateTime();
        $age = $date->diff($now);
        return $age;
    }


}

 	


<?php

namespace App\Providers;

use Validator;
use App\User;
use Illuminate\Support\ServiceProvider;

class PrivacyJsonValidationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('privacyJSON', function($attribute, $value, $parameters, $validator) {
            $validEveryone = ['allow', 'deny'];
            try {
                $data = json_decode($value, true);
            } catch (Exception $e) {
                return false;
            }
            if(isset($data['everyone']) &&     //Everyone pole nesmi byt prazdne
                isset($data['others']) &&      //Others pole nesmi byt prazdne
                in_array($data['everyone'], $validEveryone) && //Everyone musi byt allowed/denied
                (
                    array_filter($data['others'], function($value) {
                        $validOthers = ['friends'];
                        if(is_int($value))
                        {
                            try {
                                User::findOrFail($value);   //Zkusi najit uzivatele
                                return true;
                            } catch (Exception $e) {
                                return false;
                            }
                        }
                        return in_array($value, $validOthers); //Zjisti zdali to je friends
                    }, ARRAY_FILTER_USE_BOTH) ||
                    empty($data['others'])
                )
                ) 
                return true;
            return false;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App\Traits;

use Auth;

trait Privacy
{
    public function canSee($id = 0)
    {
        $id = $id != 0 ? $id : Auth::id();

        if($this->user->id == $id)
            return true;

        $allowed = $this->allowed();
        if(isset($allowed['except']) && !in_array($id, $allowed['except']))
        {
            if($allowed['allowed'] == 'friends')
            {
                
                return $this->user->isFriendWith(Auth::user());
            }
            return true;
        }
        if(!isset($allowed['except']) && in_array($id, $allowed['allowed']))
            return true;
        return false;
    }

    private function denied()
    {
        $privacy = json_decode($this->privacy);
        if($privacy->everyone == 'deny')
        {
            return array('denied' => 'everyone', 'except' => $privacy->others );
        }
        else
        {
            return array('denied' => $privacy->others);
        }
    }

    private function allowed()
    {
        $privacy = json_decode($this->privacy);
        if($privacy->everyone == 'allow')
        {
            return array('allowed' => 'everyone', 'except' => $privacy->others );
        }
        else
        {
            return array('allowed' => $privacy->others);
        }
    }
}
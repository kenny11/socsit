<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cmgmyr\Messenger\Traits\Messagable;
use Hootlex\Friendships\Traits\Friendable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Messagable, Friendable, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','username', 'email', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Post relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    /**
     * Comment relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    /**
     * Image relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function availablePosts($id = 0, $start = 0, $count = 20)
    {
        if($id != 0)
        {
            $user = User::findOrFail($id);
            $posts = $user->posts;
            $posts = $posts->filter(function ($post, $key) {
                return $post->canSee();
            });
            return $posts->splice($start, $count)->sortByDesc('created_at');
        }
        $posts = $this->posts;
        $friends = $this->getFriends();
        foreach ($friends as $friend) 
        {
            $posts = $posts->merge($friend->posts);                
        }
        $posts = $posts->filter(function ($post, $key) {
            return $post->canSee();
        });
        return $posts->splice($start, $count)->sortByDesc('created_at');
    }

    public function profileImage()
    {
        $result = $this->images->where('type', 'profile');
        if($result->count()>0)
            $result = $result->last()->name;
        else
            $result = config('image.defaultAvatar');
        return $result;
    }
}

<?php

return [
	'path'				=>	'uploads/images',
	'nameLength'		=>	20,
	'defaultAvatar'	=>	'avatar.png',
];
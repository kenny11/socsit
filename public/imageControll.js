$(document).ready(function() {
	$('input[name=image][type=file]').on('change', function(event) {
		event.preventDefault();
		fileName = $(this).val();
		$('input[name=image_name]').val(fileName);
	});
});
var sendAjaxRequest, reload;
$(document).ready(function() {
	$('.message .close').on('click', function() {
		$(this).closest('.message').transition('fade');
	});
	$('select.dropdown').dropdown();
	$('.menu .item').tab();
	reload = function()
	{
		history.go(0);
	}
	$('form[data-ajax="true"]').each(function(){
		$(this).submit(function(e){
			sendAjaxRequest(e);
		});
	});
	sendAjaxRequest = function(e)
	{
        e.preventDefault();
		var form = $(e.target);
        var formData = new FormData(e.target);
		$.ajaxSetup(
		{
		    headers:
		    {
		        'X-CSRF-Token': form.children('input[name="_token"]').val()
		    }
		});
        $.ajax({
            type:'POST',
            url: form.attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
                if(form.data('callback'))
                    $("body").append("<script type='text/javascript'>"+form.data('callback')+"("+JSON.stringify(data)+");</script>");
            },
            error: function(data){
                console.log("error");
                console.log(data);
            }
        });
	}
});


$(document).ready(function() {
	$('.custom.comments').css('display', 'none');


	$('.post.delete').on('click', function(e) {
		e.preventDefault();
		url = $(this).attr('href');
		var element = $(this);
		$('.delete.modal').modal({
			closable  : false,
			onApprove : function(){
				$.ajax({
				    url: url,
				    type: 'DELETE'
				}).done(function(result){
					$('.response.modal .header').text('Success');
					$('.response.modal .content').text('Successfully removed post.');
			        $('.response.modal').modal('show');
			        element.closest('.post.segment').fadeOut('slow', function() {
			        	$(this).remove();
			        });
				}).fail(function(result, error){
					$('.response.modal .header').text('Error');
					$('.response.modal .content').text('Something went wrong, please try it again later.');
			        $('.response.modal').modal('show');
				});
			}
		}).modal('show');
	});
	
	$('.post.edit').on('click', function(e) {
		//e.preventDefault();
	});
	
	$('.custom.comment').on('click', function(e) {
		e.preventDefault();
		var comments = $(this).parent().parent().children('.comments');
		if(comments.css('display') == 'none')
			comments.slideDown('400');
		else
			comments.slideUp('400');
	});
	$('.ui.button.tab').on('click', function() {
		var tab = $(this).data('tab');
    	$.tab('change tab', tab);
  	});
  	var uploaded = false;
  	$('.form[data-type=image]').on('submit', function(e) {
  		if(uploaded)
			return;
  		else
  			e.preventDefault();

  		var form = $(this);
  		var button = $(this).closest('.button[type=submit]');
  		button.addClass('loading');
  		var formData = new FormData();
		formData.append('image', $('input[type=file]')[0].files[0]);
		formData.append('privacy', $('input[name=privacy]').val());
		formData.append('type', 'post');
		$.ajaxSetup(
		{
		    headers:
		    {
		        'X-CSRF-Token': $(this).children('input[name="_token"]').val()
		    }
		});
	    $.ajax({
	        url: 'image',
	        type: $(this).attr('method'),
	        data: formData,
	        cache: false,
	        contentType: false,
	        processData: false
	    }).done(function(res){
	    	
	    	button.removeClass('loading');
	    	var data = {image:res.image.id};
	    	form.children('input[name=data]').val(JSON.stringify(data));
	    	uploaded = true;
	    	$(form).submit(function(e){sendAjaxRequest(e);});
	    	form.trigger('submit');
	    }).fail(function(e){
	    	console.log(e)
	    	//location.reload();
	    });
  	});
});

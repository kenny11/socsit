@extends('app.boilerplate')
@section('title')
	Create comment
@endsection
@section('content')

@include('app.errors.list')
	<form  method="POST" action="{{ URL::to('/comment') }}" class="ui form text container">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="field"> 
			<label>{{ trans('site/comment.text') }}</label>
			<textarea class="form-control" name="text" placeholder="What is on your mind?" maxlength="255" required="" rows="2">{{ old('text') }}</textarea>
		</div>
		<div class="field">
			<input type="number" name="post_id" placeholder="0" required="">
		</div>
		<div class="field">
			<button type="submit" class="ui fluid teal button" style="margin-right: 15px;">
				{{ trans('site/comment.BTN-post') }}
			</button>
		</div>
	</form>
@endsection
@extends('app.boilerplate')
@section('title')
	Edit post
@endsection
@section('content')

@include('app.errors.list')
	<form  method="POST" action="{{ URL::to('/comment/'.$comment->id) }}" class="ui form text container">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="_method" value="PUT">
		<div class="field">
			<label>{{ trans('site/post.text') }}</label>
			<textarea class="form-control" name="text" placeholder="What is on your mind?" maxlength="255" required="" rows="2">{{ $comment->text }}</textarea>
		</div>
		<div class="field">
			<button type="submit" class="ui fluid teal button" style="margin-right: 15px;">
				{{ trans('site/comment.BTN-update') }}
			</button>
		</div>
	</form>
@endsection


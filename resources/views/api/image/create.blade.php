@extends('app.boilerplate')
@section('title')
	Upload image
@endsection
@section('scripts')
@parent
<script src="{{URL::to('/imageControll.js')}}" type="text/javascript"></script>
@endsection
@section('content')

@include('app.errors.list')
	<form method="POST" action="{{ URL::to('/image') }}" class="ui form text container" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" class="form-control" name="type" value="text">
		<div class="two fields">
			<div class="field">
				<input type="text" name="image_name" placeholder="Image name" disabled="">
			</div>
			<div class="field">
				<label class="ui icon fluid teal button" for="image">
					<i class="file icon"></i>
			        Open File
				</label>
			    <input type="file" id="image" name="image" style="display:none">
			</div>
		</div>
		<div class="field">
			<label>Type</label>
			<select name="type" class="ui dropdown" required="">
				<option value="post">post</option>
				<option value="profile">profile</option>
			</select>
		</div>
		<div class="field">
			<label>{{ trans('site/image.privacy') }}</label>
			<textarea class="form-control" name="privacy" placeholder="Just privacy things" maxlength="255" required="" rows="2">{{ old('privacy') }}</textarea>
		</div>
		<div class="field"> 
			<button type="submit" class="ui fluid teal button">
				{{ trans('site/image.BTN-upload') }}
			</button>
		</div>
	</form>
@endsection
@extends('app.boilerplate')
@section('title')
	Edit image
@endsection
@section('content')

@include('app.errors.list')
	<form  method="POST" action="{{ URL::to('/image/'.$image->id) }}" class="ui form text container">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" class="form-control" name="type" value="text">
		<div class="field">
			<label>{{ trans('site/post.text') }}</label>
			<textarea class="form-control" name="text" placeholder="What is on your mind?" maxlength="255" required="" rows="2">{{ $post->text }}</textarea>
		</div>
		<div class="field">
			<label>{{ trans('site/post.privacy') }}</label>
			<textarea class="form-control" name="privacy" placeholder="Just privacy things" maxlength="255" required="" rows="2">{{ $post->privacy }}</textarea>
		</div>
		<div class="field">
			<button type="submit" class="ui fluid teal button" style="margin-right: 15px;">
				{{ trans('site/post.BTN-update') }}
			</button>
		</div>
	</form>
@endsection


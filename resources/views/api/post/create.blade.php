@extends('app.boilerplate')
@section('title')
	Create post
@endsection
@section('content')

@include('app.errors.list')
	<form  method="POST" action="{{ URL::to('/post') }}" class="ui form text container">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" class="form-control" name="type" value="text">
		<div class="field">
			<label>{{ trans('site/post.text') }}</label>
			<textarea class="form-control" name="text" placeholder="What is on your mind?" maxlength="255" required="" rows="2">{{ old('text') }}</textarea>
		</div>
		<div class="field">
			<label>{{ trans('site/post.privacy') }}</label>
			<textarea class="form-control" name="privacy" placeholder="Just privacy things" maxlength="255" required="" rows="2">{{ old('privacy') }}</textarea>
		</div>
		<div class="field"> 
			<button type="submit" class="ui fluid teal button" style="margin-right: 15px;">
				{{ trans('site/post.BTN-post') }}
			</button>
		</div>
	</form>
@endsection
@extends('app.boilerplate')
@section('title')
	Edit thread
@endsection
@section('content')

@include('app.errors.list')
	<form  method="POST" action="{{ URL::to('/thread/'.$thread->id) }}" class="ui form text container">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" class="form-control" name="type" value="text">
		<div class="field">
			<label>{{ trans('site/thread.subject') }}</label>
			<input type="text" name="subject" placeholder="Thread name" value="{{ $thread->subject }}" maxlength="255">
		</div>
		<div class="field">
			<label>{{ trans('site/thread.message') }}</label>
			<textarea class="form-control" name="message" placeholder="Type your message here" maxlength="255" required="" rows="2">{{ old('message') }}</textarea>
		</div>
	    @if($users->count() > 0)
		    <div class="ui checkbox">
		        @foreach($users as $user)
		            <input type="checkbox" name="recipients[]" value="{!!$user->id!!}">
		            <label title="{!!$user->name!!}">{!!$user->name!!}</label>
		        @endforeach
		    </div>
	    @endif

		<div class="field">
			<button type="submit" class="ui submit button" style="margin-right: 15px;">
				{{ trans('site/thread.BTN-update') }}
			</button>
		</div>
	</form>
@endsection


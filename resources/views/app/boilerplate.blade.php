<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		SocSit - @yield('title')
		
	</title>
    @section('styles')
        @include('app.styles')
    @show
</head>
<body>
	@section('scripts')
        @include('app.scripts')
    @show
    @include('app.header')
    <div class="ui main text container">
    	@yield('content')
    </div>
    @include('app.footer')
</body>
</html>
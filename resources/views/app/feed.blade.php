<div class="ui small basic delete modal">
	<div class="ui icon header">
		<i class="trash icon"></i>
		Delete post
	</div>
	<div class="content">
		Do you really want to delete this post?
	</div>
	<div class="actions">
		<div class="ui cancel inverted button">
			<i class="remove icon"></i>
			Cancel
		</div>
		<div class="ui red approve inverted button">
        	<i class="checkmark icon"></i>
        	Delete
      	</div>
	</div>
</div>
<div class="ui small basic response modal">
	<div class="ui icon header"></div>
	<div class="content"></div>
	<div class="actions">
		<div class="ui cancel inverted button">Close</div>
	</div>
</div>
@foreach(Auth::user()->availablePosts() as $post)
	@include('post.post')
@endforeach
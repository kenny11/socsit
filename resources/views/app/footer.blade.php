<div class="ui inverted vertical footer segment">
	<div class="ui center aligned container">
		<div class="ui horizontal inverted small link list">
			<a class="item" href="http://portfolio.growie.eu/">
				Created by <span class="ui blue header">Dan Balarin</span>
			</a>
		</div>
		<div class="ui inverted section small divider"></div>
		<div class="ui horizontal inverted small divided link list">
			<a class="item" href="#">Terms and Conditions</a>
			<a class="item" href="#">Privacy Policy</a>
			<a class="item" href="http://www.freepik.com">Avatars designed by Freepik</a>
			<a class="item" href="http://semantic-ui.com/">
				<img style="" title="" src="http://semantic-ui.com/images/logo.png" class="ui mini image">
				&nbsp;
				Build with Semantic
			</a>
			
		</div>
	</div>
</div>
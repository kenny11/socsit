<div class="ui fixed menu">
	<div class="ui container">
		<a href="{{URL::to('/')}}" class="header item">
			<img class="logo" src="">
			SocSit
		</a>
	
		<div class="right menu">
			@if(Auth::check())
				<div class="item">
					<div class="ui action left icon input">
						<i class="search icon"></i>
						<input placeholder="Search" type="text">
						<button class="ui button">Submit</button>
					</div>
				</div>
				<a href="{{URL::to('/logout')}}" class="item">Log out</a>
			@else
				<a href="{{URL::to('/login')}}" class="item">Log in</a>
			@endif
		</div>
	</div>
</div>
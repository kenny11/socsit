@extends('app.boilerplate')
@section('title')
    Login
@endsection
@section('content')
<div class="ui middle aligned center aligned grid">
    <form class="ui form fixed-width"  method="POST" action="{{ url('/login') }}">
        <h2 class="ui teal center aligned header">Log into your account</h2>
        {{ csrf_field() }}

        <div class="field{{ $errors->has('username') ? ' error' : '' }}">
            <div class="ui left icon input">
                <i class="user icon"></i>
                <input name="username" placeholder="Username" type="text" value="{{ old('username') }}" required="">
            </div>
            @if ($errors->has('username'))
                <div class="ui pointing red basic label">
                  {{ $errors->first('username') }}
                </div>
            @endif
        </div>

        <div class="field{{ $errors->has('password') ? ' error' : '' }}">
            <div class="ui left icon input">
                <i class="lock icon"></i>
                <input type="password" name="password" value="{{ old('password') }}" required="">
            </div>

            @if ($errors->has('password'))
                <div class="ui pointing red basic label">
                  {{ $errors->first('password') }}
                </div>
            @endif
        </div>

        <div class="ui checkbox field">
            <input name="remember" type="checkbox">
            <label> Remember Me</label>
        </div>
        <div class="field">
            <button type="submit" class="ui fluid teal button">
                Login
            </button>   
        </div>
        <div class="two fields">
            <div class="field">
                <a class="ui fluid button" href="{{ url('/password/reset') }}">
                    Forgot password?
                </a>
            </div>
            <div class="field">
                <a class="ui fluid button" href="{{ url('/register') }}">
                    Not member?
                </a>
            </div>
        </div>  
    </form>
</div>


@endsection

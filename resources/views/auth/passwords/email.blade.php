@extends('app.boilerplate')
@section('title')
    Reset password
@endsection
@section('content')
<div class="ui middle aligned center aligned grid">
    <div class="panel-body">
        @if (session('status'))
            <div class="ui success message">
                <div>{{ session('status') }}</div>
            </div>
        @endif
        <form class="ui form fixed-width"  method="POST" action="{{ url('/password/email') }}">
            <h2 class="ui teal center aligned header">Reset password</h2>
            {{ csrf_field() }}
            <div class="field{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="ui left icon input">
                    <i class="fa fa-envelope icon"></i>
                    <input id="email" type="email" name="email" placeholder="E-Mail address" value="{{ old('email') }}" required="">
                </div>
                @if ($errors->has('email'))
                    <div class="ui pointing red basic label">
                      {{ $errors->first('email') }}
                    </div>
                @endif
            </div>
            <div class="field">
                <button type="submit" class="ui teal fluid button">
                     Send Password Reset Link
                </button>
            </div>
        </form>
    </div>
</div>
@endsection

@extends('app.boilerplate')
@section('title')
    Set new password
@endsection
@section('content')
<div class="ui middle aligned center aligned grid">
    <form class="ui form fixed-width"  method="POST" action="{{ url('/password/reset') }}">
        <h2 class="ui teal center aligned header">Set new password</h2>
        {{ csrf_field() }} 
        <input type="hidden" name="token" value="{{ $token }}">

        <div class="required field{{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="ui left icon input">
                <i class="fa fa-envelope icon"></i>
                <input type="email" name="email" placeholder="E-Mail address" value="{{ $email or old('email') }}" required="">
            </div>
            @if ($errors->has('email'))
                <div class="ui pointing red basic label">
                  {{ $errors->first('email') }}
                </div>
            @endif
        </div>

        <div class="two fields">
            <div class="required field{{ $errors->has('password') ? ' has-error' : '' }}">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="Password" required="">
                </div>
                @if ($errors->has('password'))
                    <div class="ui pointing red basic label">
                      {{ $errors->first('password') }}
                    </div>
                @endif
            </div>

            <div class="required field{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" class="form-control" name="password_confirmation"    placeholder="Confirm Password" required="">
                </div>
                @if ($errors->has('password_confirmation'))
                    <div class="ui pointing red basic label">
                      {{ $errors->first('password_confirmation') }}
                    </div>
                @endif
            </div>
        </div>
        <div class="field">
            <button type="submit" class="ui fluid teal button">
                Set new password
            </button>
        </div>
    </form>
</div>
@endsection

@extends('app.boilerplate')
@section('title')
    Register
@endsection
@section('content')
<div class="ui middle aligned center aligned grid">
    <form class="ui form fixed-width"  method="POST" action="{{ url('/register') }}">
        <h2 class="ui teal center aligned header">Create new account</h2>
        {{ csrf_field() }}
        <div class="required field{{ $errors->has('name') ? ' error' : '' }}">
            <div class="ui left icon input">
                <i class="child icon"></i>
                <input type="text" name="name" placeholder="Name" value="{{ old('name') }}" required="">
            </div>
            @if ($errors->has('name'))
                <div class="ui pointing red basic label">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>

        <div class="required field{{ $errors->has('username') ? ' error' : '' }}">
            <div class="ui left icon input">
                <i class="user icon"></i>
                <input type="text" name="username" placeholder="Username" value="{{ old('username') }}" required="">
            </div>
            @if ($errors->has('username'))
                <div class="ui pointing red basic label">
                    {{ $errors->first('username') }}
                </div>
            @endif
        </div>

        <div class="required field{{ $errors->has('email') ? ' error' : '' }}">
            <div class="ui left icon input">
                <i class="fa fa-envelope icon"></i>
                <input type="email" name="email" placeholder="E-Mail address" value="{{ old('email') }}" required="">
            </div>
            @if ($errors->has('email'))
                <div class="ui pointing red basic label">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>
        <div class="two fields">
            <div class="required field{{ $errors->has('password') ? ' error' : '' }}">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password" placeholder="Password" required="">
                </div>
                @if ($errors->has('password'))
                    <div class="ui pointing red basic label">
                        {{ $errors->first('password') }}
                    </div>
                @endif
            </div>

            <div class="required field{{ $errors->has('password_confirmation') ? ' error' : '' }}">
                <div class="ui left icon input">
                    <i class="lock icon"></i>
                    <input type="password" name="password_confirmation" placeholder="Confirm password" required="">
                </div>
                @if ($errors->has('password_confirmation'))
                    <div class="ui pointing red basic label">
                        {{ $errors->first('password_confirmation') }}
                    </div>
                @endif
            </div>       
        </div>
        <div class="field">
            <button type="submit" class="ui fluid teal button">
                <i class="fa fa-user"></i> Register
            </button>
        </div>
    </form>
</div>
@endsection

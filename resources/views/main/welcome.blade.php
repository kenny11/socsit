@extends('app.boilerplate')
@section('title', 'Welcome')
@section('scripts')
@parent
<script src="{{URL::to('postControll.js')}}"></script>
@endsection
@section('content')
@if(Auth::check())
	@include('main.user')
@else
	@include('main.guest')
@endif
@endsection
<div class="comment">
	<a class="avatar">
		<img src="{{Utils::pathToImage($post->user->profileImage())}}" alt="{{$comment->user->name}}">
	</a>
	<div class="content">
		<a class="author">{{$comment->user->name}}</a>
		<div class="metadata">
			<span class="date">{{Utils::dateToText($comment->created_at)}}</span>
		</div>
		<div class="text">
			{{$comment->text}}
		</div>
		<div class="actions">
			@if($post->isAuthor())
				<a href="{{URL::to('comment/'.$comment->id.'/edit')}}" class="comment edit ui yellow empty circular label"></a>
				<a href="{{URL::to('comment/'.$comment->id)}}" class="comment delete ui red empty circular label"></a>
			@endif
	    </div>
	</div>
</div>	
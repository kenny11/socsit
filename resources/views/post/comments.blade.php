<div class="custom comments">
	<div class="ui divider"></div>
	@if($post->comments->count() > 0)
		<div class="ui comments">
			@foreach($post->comments as $comment)
				@include('post.comment')
			@endforeach	
		</div>
	@else
		<h2 class="ui teal header">Nothing to show :(</h2>
	@endif
</div>
@section('scripts')
@parent
<script src="{{URL::to('/imageControll.js')}}" type="text/javascript"></script>
@endsection
<div class="ui segment">
	@include('app.errors.list')
	<div class="ui grid">
		<div class="one wide column">
			<div class="ui vertical buttons">
				<button class="ui icon button active tab" data-tab="text"><i class="fa fa-pencil icon"></i></button>
				<button class="ui icon button tab" data-tab="image"><i class="file image outline icon"></i></button>
			</div>
		</div>
		<div class="fifteen wide column">
			<div class="ui tab active" data-tab="text">
				<form  method="POST" action="{{ URL::to('/post') }}" class="ui form" data-ajax="true" data-callback="reload">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" class="form-control" name="type" value="text">
					<input type="hidden" name="privacy" value='{"everyone":"allow","others":[]}'>
					<div class="field">
						<textarea class="form-control" name="text" placeholder="What is on your mind?" maxlength="255" required="" rows="2">{{ old('text') }}</textarea>
					</div>
					<div class="field"> 
						<button type="submit" class="ui fluid teal button" style="margin-right: 15px;">
							{{ trans('site/post.BTN-post') }}
						</button>
					</div>
				</form>
			</div>
			<div class="ui tab" data-tab="image">
			  	<form method="POST" action="{{ URL::to('/post') }}" class="ui form" data-type="image" enctype="multipart/form-data" data-ajax="false" data-callback="reload">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" class="form-control" name="type" value="text">
					<input type="hidden" name="type" value="image">
					<input type="hidden" name="privacy" value='{"everyone":"allow","others":[]}'>
					<input type="hidden" name="data" value="">
					<div class="field">
						<textarea class="form-control" name="text" placeholder="What is on your mind?" maxlength="255" required="" rows="2">{{ old('text') }}</textarea>
					</div>
					<div class="two fields">
						<div class="field">
							<input type="text" name="image_name" placeholder="Image name" disabled="">
						</div>
						<div class="field">
							<label class="ui icon fluid teal button form" for="image">
								<i class="file icon"></i>
						        Open File
							</label>
						    <input type="file" id="image" name="image" style="display:none" required="">
						</div>
					</div>
					<div class="field"> 
						<button type="submit" class="ui fluid teal button">
							{{ trans('site/image.BTN-upload') }}
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>


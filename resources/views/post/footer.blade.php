
<div class="ui divider"></div>
<div class="ui item">
	<div class="ui label">
		<i class="thumbs up icon"></i> 0
	</div>
	<div class="custom comment ui right floated mini button">
		Comments {{$post->comments->count()}}
	</div>
</div>
@include('post.comments')



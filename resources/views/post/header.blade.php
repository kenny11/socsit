<div class="">
	<div class="ui three column grid">
		<div class="column author">
			<div class="ui comments">
				<div class="comment">
					<a class="avatar">
					
						<img src="{{Utils::pathToImage($post->user->profileImage())}}" alt="{{$post->user->name}}">
					</a>
					<div class="content">
						<a class="author">{{$post->user->name}}</a>
						<div class="metadata">
							<span class="date">{{Utils::dateToText($post->created_at)}}</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="right floated right aligned column">
			@if($post->isAuthor())
				<a href="{{URL::to('post/'.$post->id.'/edit')}}" class="post edit ui yellow empty circular label"></a>
				<a href="{{URL::to('post/'.$post->id)}}" class="post delete ui red empty circular label"></a>
			@endif
		</div>
	</div>
</div>
<div class="ui divider"></div>



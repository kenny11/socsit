<div class="ui segment post">
	@include('post.header')
	<div class="content">
		{{$post->text}}
		@if($post->type == 'image')
			<?php
				$image = App\Image::find(json_decode($post->data)->image);
			?>
			<div class="image">
				<img title="" src="{{URL::to(config('image.path').'/'.$image->name)}}">
			</div>
		@endif
	</div>
	@include('post.footer')
</div>
	
